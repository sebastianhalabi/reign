# Hacker News - Reign

This project has been created as a task for reign.cl.
It uses ReactJS + Typescript.
It consists on a simple web app that visualizes news consumed from the API https://hn.algolia.com/api.
It lets the user filter the news by keywords and save news to favorites.

## URL to the Project

You can find an online version of this Git repository on:
https://wonderful-noyce-8b1d6d.netlify.app

## Available Scripts

In the project directory, you can run:

### `npm start`

Runs the app in the development mode.\
Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

### `npm test`

Launches the test runner in the interactive watch mode.\
Tests have been made with Jest + TS-jest.

### `npm run build`

Builds the app for production to the `build` folder.\
It correctly bundles React in production mode and optimizes the build for the best performance.

The build is minified and the filenames include the hashes.\
Your app is ready to be deployed!

This section has moved here: [https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify](https://facebook.github.io/create-react-app/docs/troubleshooting#npm-run-build-fails-to-minify)
