const timeSince = require('../aux/timeSince');
const getNewsAPI = require('../API/newsAPI');

test('returns a valid string', () => {
  expect(typeof timeSince('2021-10-29T13:43:13.000Z')).toBe('string');
});

test('API call brings data', async () => {
  var _res = await getNewsAPI('angular', 1);
  expect(typeof _res.hits).toBe('object');
});
