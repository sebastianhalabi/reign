import Home from './views/home/home';
import './App.css';

function App() {
  return (
    <div className='App'>
      <header className='App-header'>
        <Home />
      </header>
    </div>
  );
}

export default App;
