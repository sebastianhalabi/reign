import { useState } from 'react';
import angularLogo from '../../assets/angular.png';
import reactLogo from '../../assets/react.png';
import vueLogo from '../../assets/vue.png';
import getNewsAPI from '../../API/newsAPI';
import { message } from 'antd';

const HomeController = () => {
  const [favorites, setFavorites] = useState<Array<any>>([]);
  const [allNews, setAllNews] = useState<Array<any>>([]);
  const [filter, setFilter] = useState<string>('angular');
  const [loading, setLoading] = useState<boolean>(true);
  const [currentPage, setCurrentPage] = useState<number>(1);
  const [currentView, setCurrentView] = useState<'all' | 'favorites'>('all');

  const selectOptions = [
    { value: 'angular', text: 'Angular', logo: angularLogo },
    { value: 'react', text: 'ReactJS', logo: reactLogo },
    { value: 'vue', text: 'VueJS', logo: vueLogo },
  ];

  const viewsOptions = [
    { value: 'all', text: 'All' },
    { value: 'favorites', text: 'My faves' },
  ];

  const initialize = async () => {
    //If there is any filter saved in localStorage, set it as the current filter
    let filterFromLocal: string = localStorage.getItem('filter') || '';
    if (filterFromLocal) {
      setFilter(filterFromLocal);
    }
    await getNews(filterFromLocal ?? filter, currentPage);
    getFavoritesFromLocal();
  };

  const getFavoritesFromLocal = () => {
    var values = [],
      keys = Object.keys(localStorage),
      i = keys.length;
    while (i--) {
      if (keys[i] !== 'filter')
        //brings all the values from localStorage, except for the filter
        values.push(JSON.parse(localStorage.getItem(keys[i]) || '{}'));
    }
    setFavorites(values);
  };

  const getNews = async (filter: string, page: number) => {
    setLoading(true);
    let _res = await getNewsAPI(filter, page);
    if (_res) {
      setAllNews(discardIncompleteNews(_res.hits));
    } else {
      message.error(
        'There seems to be a connection problem. Please try again.',
        3
      );
    }
    setLoading(false);
  };

  const changeFilter = async (newValue: string) => {
    setFilter(newValue);
    localStorage.setItem('filter', newValue);
    setCurrentPage(1); //When filter changes, it goes back to page 1.
    await getNews(newValue, 1);
  };

  const changeCurrentPage = async (newPage: number) => {
    setCurrentPage(newPage);
    getNews(filter, newPage);
  };

  const changeCurrentView = (newView: 'all' | 'favorites') => {
    getFavoritesFromLocal(); //When view is changed, it reloads favorites
    setCurrentView(newView);
  };

  //If any of the fields required on visualization is missing, discard it.
  const discardIncompleteNews = (news: Array<any>) => {
    let finalNews = news.filter(
      (news) =>
        news['author'] !== null &&
        news['created_at'] !== null &&
        news['tory_title'] !== null &&
        news['story_url'] !== null
    );
    return finalNews;
  };

  return {
    favorites,
    allNews,
    loading,
    currentView,
    selectOptions,
    viewsOptions,
    initialize,
    filter,
    changeFilter,
    currentPage,
    changeCurrentPage,
    changeCurrentView,
  };
};

export default HomeController;
