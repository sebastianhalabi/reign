import { Layout, Pagination, Row, Spin } from 'antd';
import { useEffect } from 'react';
import CustomHeader from '../../components/organisms/customHeader';
import { Content } from 'antd/lib/layout/layout';
import CustomRadio from '../../components/molecules/customRadio';
import CustomSelect from '../../components/molecules/customSelect';
import NewsList from '../../components/organisms/newsList';
import HomeController from '../../views/home/homeController';

const Home = () => {
  const {
    initialize,
    favorites,
    allNews,
    loading,
    currentView,
    selectOptions,
    filter,
    changeFilter,
    currentPage,
    changeCurrentPage,
    changeCurrentView,
    viewsOptions,
  } = HomeController();

  useEffect(() => {
    initialize();
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  return (
    <Layout
      className='layout background-primary'
      style={{ minHeight: '100vh' }}
    >
      <CustomHeader />
      <Content className='pxl'>
        <CustomRadio
          value={currentView}
          onChange={changeCurrentView}
          options={viewsOptions}
        />
        <Row className='ptm pbm'>
          {currentView === 'all' ? (
            <CustomSelect
              options={selectOptions}
              placeholder='Select your news'
              value={filter}
              onChange={(value: string) => changeFilter(value)}
            />
          ) : null}
        </Row>
        {loading ? (
          <Spin />
        ) : (
          <>
            <NewsList news={currentView === 'all' ? allNews : favorites} />
            <Pagination
              onChange={(e: any) => changeCurrentPage(e)}
              current={currentPage}
              defaultCurrent={1}
              total={currentView === 'all' ? 180 : favorites.length} //If current view is 'All', only 9 pages of 20 (180 items) items each will be available to visualize.
              pageSize={20}
              showSizeChanger={false}
              showLessItems={false}
              className='mtm'
            />
          </>
        )}
      </Content>
    </Layout>
  );
};

export default Home;
