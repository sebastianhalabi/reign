import axios from 'axios';

const getNewsAPI = async (filter: string, page: number): Promise<any> => {
  try {
    let _res = await axios.get(
      `https://hn.algolia.com/api/v1/search_by_date?query=${filter}&page=${page}`
    );
    if (_res.status === 200) {
      return _res.data;
    }
    throw Error(_res.data);
  } catch (error) {
    console.log('There was an error while getting the news', error);
  }
};

export default getNewsAPI;

export { getNewsAPI };
