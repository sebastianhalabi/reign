import React from 'react';
import { Image, Row, Select } from 'antd';
import Text from 'antd/lib/typography/Text';

const { Option } = Select;
const CustomSelect = ({
  options,
  placeholder,
  value,
  onChange,
}: {
  options: Array<any>;
  placeholder: string;
  value: string;
  onChange: (value: string) => Promise<void>;
}) => {
  const selectOptions = options.map((option) => (
    <Option value={option.value} key={option.value}>
      <Row align='middle'>
        <Image width={20} src={option.logo} className='prxs' preview={false} />
        <Text>{option.text}</Text>
      </Row>
    </Option>
  ));

  return (
    <Select
      placeholder={placeholder}
      style={{ width: '200px' }}
      value={value}
      onChange={onChange}
    >
      {selectOptions}
    </Select>
  );
};

export default CustomSelect;
