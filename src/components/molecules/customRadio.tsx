import { Radio } from 'antd';

const CustomRadio = ({
  value,
  options,
  onChange,
}: {
  value: 'all' | 'favorites';
  options: Array<any>;
  onChange: (e: 'all' | 'favorites') => void;
}) => {
  const radioOptions = options.map((option) => (
    <Radio.Button key={option.value} value={option.value}>
      {option.text}
    </Radio.Button>
  ));

  return (
    <Radio.Group
      defaultValue={options[0].value}
      value={value}
      onChange={(e: any) => onChange(e.target.value)}
    >
      {radioOptions}
    </Radio.Group>
  );
};

export default CustomRadio;
