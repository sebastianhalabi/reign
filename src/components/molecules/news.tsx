import { useState, useEffect } from 'react';
import { Col, Image, Row } from 'antd';
import Text from 'antd/lib/typography/Text';
import timeIcon from '../../assets/time.png';
import favoriteOutlined from '../../assets/favoriteOutlined.png';
import favoriteFilled from '../../assets/favoriteFilled.png';
import timeSince from '../../aux/timeSince';

const News = ({ info }: { info: any }) => {
  const [isFavorite, setFavorite] = useState(false);

  //On every rerender, check if news is saved as favorite non localStorage.
  useEffect(() => {
    if (localStorage.getItem(info.objectID)) {
      setFavorite(true);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, []);

  function saveToFavorites() {
    if (isFavorite) {
      localStorage.removeItem(info.objectID);
    } else {
      localStorage.setItem(info.objectID, JSON.stringify(info));
    }
    setFavorite(!isFavorite);
  }

  return (
    <Row className='news-card mbs'>
      <Col
        span={21}
        className='pbs pts pls prs pointer'
        onClick={() => window.open(info.story_url)}
      >
        <Row justify='start' align='middle' className='mbxs'>
          <Image
            src={timeIcon}
            style={{ height: '16px' }}
            className='prxs'
            preview={false}
          />
          <Text className='news-card-header'>
            {timeSince(info.created_at)} by {info.author}
          </Text>
        </Row>
        <Row justify='start'>
          <Text className='news-card-body'>{info.story_title}</Text>
        </Row>
      </Col>
      <Col span={3} className='background-grey'>
        <Row align='middle' justify='center'>
          <Image
            preview={false}
            src={isFavorite ? favoriteFilled : favoriteOutlined}
            style={{ width: '24px', paddingTop: '34px' }}
            className='pointer'
            onClick={() => saveToFavorites()}
          />
        </Row>
      </Col>
    </Row>
  );
};

export default News;
