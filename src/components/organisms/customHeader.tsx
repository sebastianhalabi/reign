import { Header } from 'antd/lib/layout/layout';
import Title from '../atoms/title';

const CustomHeader = () => {
  return (
    <Header className='header plxl'>
      <Title text='HACKER NEWS' />
    </Header>
  );
};

export default CustomHeader;
