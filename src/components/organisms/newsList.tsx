import { Col, Row } from 'antd';
import React from 'react';
import News from '../molecules/news';
import { nanoid } from 'nanoid';

const NewsList = ({ news }: { news: Array<any> }) => {
  const newsList = news.map((news) => {
    return (
      <Col span={12} className='gutter-row' key={nanoid(5)}>
        <News info={news} />
      </Col>
    );
  });

  return <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>{newsList}</Row>;
};

export default NewsList;
