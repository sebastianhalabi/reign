import { Row } from 'antd';
import Text from 'antd/lib/typography/Text';

const Title = ({ text }: { text: string }) => {
  return (
    <Row className='title' justify='start'>
      <Text>{text}</Text>
    </Row>
  );
};

export default Title;
